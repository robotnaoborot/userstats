PYTHONPATH=.
REQ=requirements
REQ_PRIVATE=requirements.private
PIP_CONFIG_FILE=deployments/.secrets/pip_private.conf
DOCKER_COMPOSE_FULL=deployments/docker-compose.full.yml
DOCKER_COMPOSE_ENV=deployments/docker-compose.env.yml

ifndef PYTHON
	PYTHON=python
endif
ifndef DOCKER_COMPOSE
	DOCKER_COMPOSE=docker-compose
endif
ifndef DOCKER
	DOCKER=docker
endif
ifndef PYTEST
	PYTEST=pytest
endif
ifndef PIP
	PIP=pip
endif
ifndef TEST_SUBFOLDER
	TEST_SUBFOLDER=./
endif
ifndef VAULT_ENV
	VAULT_ENV=LOCAL
endif
ifeq ($(TEST),yes)
	VAULT_ENV_FILE=${VAULT_ENV_FILE_TEST}
endif


.PHONY: config build publish clean deps run run-env run-full run-rebuild


ENVS=PYTHONPATH=${PYTHONPATH} VAULT_ENV_FILE=${VAULT_ENV_FILE} VAULT_ENV=${VAULT_ENV}
ENVS_TEST=PYTHONPATH=${PYTHONPATH} VAULT_ENV_FILE=${VAULT_ENV_FILE_TEST} VAULT_ENV=${VAULT_ENV}


build-image:
	$(ENVS) $(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FULL) build

publish-image:
	$(ENVS) $(DOCKER) tag $(TAG) 84.201.149.110:443/userstats:$(VERSION)
	$(ENVS) $(DOCKER) push 84.201.149.110:443/userstats:$(VERSION)

publish-package:
	$(ENVS) $(PYTHON) setup.py bdist_wheel upload -r userstats

clean:
	rm -rf build/``
	rm -rf dist/
	rm -rf user   stats.egg-info
	rm -rf data/test/temp/

deps:
	$(ENVS) $(PIP) install -r $(REQ)
	$(ENVS) PIP_CONFIG_FILE=${PIP_CONFIG_FILE} $(PIP) install -r $(REQ_PRIVATE)

run:
	$(ENVS) python userstats/server.py

run-full:
	$(ENVS) $(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FULL) up

run-env:
	$(ENVS) $(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_ENV) up

run-rebuild:
	$(ENVS) $(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FULL) up --build

