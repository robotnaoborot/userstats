from typing import Set, Optional

from pydantic import (
    BaseModel,
    BaseSettings,
    PyObject,
    RedisDsn,
    Field,
    AnyUrl,
)


class Settings(BaseSettings):
    auth: str = ''
    google_creds: str = ''
    spreadsheet: str = ''

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"

    def creds(self):
        from google_auth_oauthlib.flow import InstalledAppFlow
        import pickle, os
        if os.path.exists('/tmp/c'):
            return pickle.load(open('/tmp/c', 'rb'))
        SCOPES = ['https://www.googleapis.com/auth/drive.file', 'https://www.googleapis.com/auth/spreadsheets']
        flow = InstalledAppFlow.from_client_secrets_file(
            'credentials.json', SCOPES)
        creds = flow.run_local_server(port=0)
        pickle.dump(creds, open('/tmp/c', 'wb'))
        return creds


config = Settings()
