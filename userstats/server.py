import json

import aiohttp
import asyncio
import gspread_asyncio
from loguru import logger

from config import config
from gspread import WorksheetNotFound


def convert_user(user):
    stats = user.pop('statistics')
    user['solved'] = sum([row['solved_virtual']+row['solved_real'] for row in stats], 0)
    user['amounts'] = sum([row['amounts_virtual']+row['amounts_real'] for row in stats], 0)
    return user


@logger.catch
async def merge_to_sheet(course_id, data):
    data = data['users']
    agcm = gspread_asyncio.AsyncioGspreadClientManager(config.creds)
    agc = await agcm.authorize()
    ss = await agc.open(config.spreadsheet)
    try:
        worksheet = await ss.worksheet(course_id)
    except WorksheetNotFound:
        worksheet = await ss.add_worksheet(title=course_id, cols=11, rows=1)
        await worksheet.append_row(['id', 'first_name', 'last_name', 'cell_phone', 'email', 'active', 'course_id', 'created', 'updated', 'solved', 'amounts'])

    old_data = await worksheet.get_all_values()
    old_data_by_id = {r[0]: (i, r) for i, r in enumerate(old_data)}
    for user in map(convert_user, data):
        if user['id'] in old_data_by_id:
            row_num, row = old_data_by_id[user['id']]
            for col_num, (value, old_value) in enumerate(zip(user.values(), row)):
                if value != old_value:
                    logger.info(f'Updating user {user["id"]}')
                    await worksheet.update_cell(row_num, col_num, value)
        else:
            await worksheet.append_row(list(user.values()))


@logger.catch
async def get_stats(client, course_id):
    params = {'course_id': course_id, 'start_date': '2000-01-01T00:00'}
    async with client.get(
            'https://user-statistics.stage.fless.pro/users/statistics',
            params=params,
            headers={'auth': config.auth}
    ) as resp:
        stats = await resp.json()
        await merge_to_sheet(course_id, stats)


@logger.catch
async def get_users():
    async with aiohttp.ClientSession() as client:
        async with client.get("https://user-statistics.stage.fless.pro/courses", headers={'auth': config.auth}) as resp:
            courses = await resp.json()
            logger.info(f'Got {len(courses["courses"])} users')
            for course in courses['courses']:
                await get_stats(client, course['id'])


async def main():
    while True:
        await get_users()
        await asyncio.sleep(60*60)

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
