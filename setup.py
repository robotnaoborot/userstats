import setuptools



def parse_requirements(filename):
    """ load requirements from a pip requirements file """
    lineiter = (line.strip() for line in open(filename))
    return [line for line in lineiter if line and not line.startswith("#")]


with open("README.md", "r") as fh:
    long_description = fh.read()


install_reqs = parse_requirements('./requirements')
print(install_reqs)

setuptools.setup(
    name='userstats',
    version=0.1.0,
    author='robotnaoborot',
    author_email='robotnaoborot@gmail.com',
    description='posts data from api to google sheets',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url=f'https://github.com/Hedgehogues/userstats',
    packages=setuptools.find_packages(exclude=[]),
    classifiers=[
        "Programming Language :: Python :: 3.7",
        "Operating System :: OS Independent",
    ],
    install_requires=install_reqs,
)
